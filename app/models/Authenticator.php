<?php

namespace App\Model;

class Authenticator extends \Nette\Object implements \Nette\Security\IAuthenticator {

    /** @var \Dibi\Connection */
    protected $db;

    public function __construct(\Dibi\Connection $db) {
        $this->db = $db;
    }

    public function authenticate(array $credentials) {
        list($email, $password) = $credentials;

        $row = $this->db->query('SELECT * FROM user WHERE email = %s', $email)->fetch();

        if(!$row) {
            throw new \Nette\Security\AuthenticationException('User not found.');
        }

        if(!\Nette\Security\Passwords::verify($password, $row->password)) {
            throw new \Nette\Security\AuthenticationException('Invalid password!');
        }

        return new \Nette\Security\Identity($row->id, [], $row->toArray());
    }

}
