<?php

namespace App\AdminModule\Presenters;

use Nette;


class BasePresenter extends Nette\Application\UI\Presenter {

    /** @persistent */
    public $locale;

    /** @var \Kdyby\Translation\Translator @inject */
    public $translator;

    public function beforeRender() {
        if(!$this->user->isLoggedIn() && $this->presenter != 'Account' && $this->action != 'loginScreen') $this->redirect('Account:loginScreen');
    }

}
