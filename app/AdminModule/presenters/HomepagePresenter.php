<?php

namespace App\AdminModule\Presenters;


class HomepagePresenter extends BasePresenter {

    /** @var \Dibi\Connection @inject */
    public $db;

    public function renderDefault() {
        $this->db->query('SELECT * FROM user');
        \Tracy\Debugger::barDump($this->user->getIdentity()->name);
    }

}
