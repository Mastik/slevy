<?php

namespace App\AdminModule\Presenters;


use Nette\Application\UI\Form;

class AccountPresenter extends BasePresenter {

    /** @var \App\Model\Authenticator @inject */
    public $authenticator;

    public function actionLogin($email, $password) {
        $this->user->setAuthenticator($this->authenticator);

        try {
            $this->user->login($email, $password);
        }
        catch (\Nette\Security\AuthenticationException $ex) {
            $this->flashMessage($ex->getMessage(), 'danger');
            $this->redirect('loginScreen');
        }

        $this->redirect('Homepage:default');
    }

    public function actionLogout() {
        $this->user->logout(true);
        $this->redirect(':Front:Homepage:');
    }

    public function createComponentLogin() {
        $form = new Form();

        $form->setTranslator($this->translator);

        $form->addText('email', 'Admin.Account.LoginScreen.email');

        $form->addPassword('password', 'Admin.Account.LoginScreen.password');

        $form->addSubmit('submit', 'Admin.Account.LoginScreen.login');

        $form->onSuccess[] = [$this, 'succeedLogin'];

        return $form;
    }

    public function succeedLogin(Form $form) {
        $values = $form->getValues(true);

        $this->redirect('login', $values);
    }

}